#pragma once

#include <stdexcept>

enum RootsCount { kZero, kOne, kTwo, kInf };

struct Roots {
    RootsCount count;
    double first, second;
};

Roots SolveQuadratic(int a, int b, int c) {
    throw std::runtime_error("Not implemented");
}
