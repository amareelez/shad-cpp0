#include <cstdlib>
#include <iostream>

void TryRead(int *x) {
    if (!(std::cin >> *x)) {
        std::cout << "Fail on reading" << std::endl;
        exit(0);
    }
}

int main() {
    int a;
    TryRead(&a);

    std::cout << abs(a) << std::endl;
    return 0;
}
